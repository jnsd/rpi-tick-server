#!/bin/bash

# start influxdb in background and detached from this terminal
nohup influxd -config /etc/influxdb/influxdb.conf &

# wait for the startup of influxdb
RET=1
while [[ RET -ne 0 ]]; do
    sleep 3
    curl -k http://localhost:8086/ping 2> /dev/null
    RET=$?
done

influx -execute="create database sensor_data"

# keep the container alive
tail -f /dev/null

# NOTE: influxd is only listed with "ps -ax"
