# RPi Influx

Running TICK stack (or parts of it) in docker container on Raspberry Pi, i.e. Telegraf, InfluxDB, Chronograf and Kapacitor.

## Hypriot Setup: Raspbian OS with Docker Server

1. Download the [Hypriot image](https://blog.hypriot.com/downloads/) and follow the [instructions](https://blog.hypriot.com/getting-started-with-docker-on-your-arm-device/)

2. Flash os image to sd card (e.g. using [Etcher](https://www.balena.io/etcher/))

3. *Setup wifi*: mount sd card and create file `wpa_supplicant.conf` in root folder with this content:
   **IMPORTANT**: only change the country, ssid and password - do not add tabs!

   ```bash
   country=DE
   ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
   update_config=1
   network={
   ssid="Your network SSID"
   psk="Your network's passphrase"
   key_mgmt=WPA-PSK
   }
   ```

4. *Setup ssh*: create empty `ssh` file in sd card's root folder

5. Change hostname of RPi to `some-sensor` in file `user-data`

6. OPTION: for external Waveshare hdmi lcd display: add content of `rpi/config.txt` to `config.txt` file in sd card's root folder

6. Eject sd card and power up RPi

7. Connect to the RPi with `ssh pirate@black-pearl` and password `hypriot`

8. Change password with `passwd`

## TICK Stack Setup

The TICK stack is composed of single docker containers running the services Influxdb and Chronograf so far. Containers providing Telegraf and Kapacitor might be added in the future.

How to Setup:

1. Clone the project repo `git clone git@gitlab.com:jnsd/rpi-tick-server.git` and open the project folder
2. Change the RPi's host name `sudo rpi/set-hostname.sh sensor-server` to enable Chronograf server to connect to Influxdb server
3. Build the images with `docker-compose build`
4. Run the containers with `docker-compose up -d`! As the containers are set with  `restart: always`, they will restart after failures and after docker restart, i.e. after system reboot
5. You are up and running: use your browser to open Chronograf with http://sensor-server:8888

## Tipps & Hints

- Re-create volumes
  1. `docker-compose stop`
  2. `docker-compose rm`
  3. `docker volume prune` (not sure about this)
  4. `docker-compose up`
- Use `clean.sh`to remove stopped containers and un-used images
- Start single docker containers on the host with `docker run -d <image-name>`
- Open shell in started container with `docker exec -it <container-id OR container-name> /bin/bash`
- Get docker debug info with `sudo service docker stop` and then `dockerd -D`
