#!/bin/bash

if [ $1 == "download" ]
then
  if [ $2 == "" ]
  then
    echo "missing dashboard index no"
    exit 1
  fi
  # download dashboard
  curl -i -X GET http://sensor-server:8888/chronograf/v1/dashboards/$2 > chrono-dashboard-$2.json
  exit 0
fi

if [ $1 == "upload" ]
then
  if [ $2 == "" ]
  then
    echo "missing dashboard file mame to upload"
    exit 1
  fi
  # upload dashboard
  curl -i -X POST -H "Content-Type: application/json" http://sensor-server:8888/chronograf/v1/dashboards -d @$2
  exit 0
fi

echo "usage: 'chronograf-dashboard download <index>' or 'chronograf-dashboard upload <file name>'"
